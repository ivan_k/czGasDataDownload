
# downloadCzGasDailyTemp ----------------------------------------------
#' Download average daily temperatures from OTE for given year.
#'
#' Download average daily gas temperatures from OTE for given year.
#'
#' @param years years for which user want the temperatues
#' @param tmpDir directory where the files will be downloaded
#' @param rmTmpFiles TRUE/FALSE whether remove temporaraly files dowloaded from OTE server
#'
#' @return data.frame containing 3 colums: date, avarageDailyTemperature, normalDailyTemperature
#'
#' @details The avarage dailly temperature in natural gas industry in the Czech republic is defined as weighted avarage of measured temperature at 9:00, 12:00, 21:00 (weights .25,.25,.5 respectivelly). The measurement are done by CHMI - te only thing known is that measurements are taken from station with maximal elevation around 700m.
#' The files are available in this format only from 2011. Also at the end of the current files forecast values are located, therefore last row of the returned data frame is Sys.Date()-1.
#'
#' @author Ivan Kasanicky, \email{kasanickyivan@@gmail.com}
#'
#' @references \url{http://www.ote-cr.cz/statistika/typove-diagramy-dodavek-plynu/teploty}
#'
#' @examples
#'  downloadCzGasDailyTemp('2014')
#'
#'
#' @import dplyr
#' @import readxl
#' @export
downloadCzGasDailyTemp <- function(years = integer(0),tmpDir = '.',rmTmpFiles = T){
  allUrls <- getOteFileUrl(years)

  gasTemp <- data_frame()

  if ( length(allUrls) ){
    for ( i in 1:length(allUrls) ){
      fileUrl <- allUrls[i]
      parts <- regexec("^([[:print:]]*)/([[:alnum:]_.]*)", fileUrl) %>%
        regmatches(fileUrl,.) %>%
        unlist()
      zipFile <- file.path( tmpDir , parts[3] )
      xlsFile <- sub('.zip','.xls',parts[3]) %>%
        file.path( tmpDir , . )
      download.file(fileUrl,zipFile)
      unzip(zipFile)
      gasTemp <- readxl::read_excel( path = xlsFile , col_names = F , skip = 6,
                        sheet = 'Teploty plyn',
                        col_types = c('date','numeric','numeric','numeric')) %>%
        dplyr::mutate( gasDate = as.Date(X1) ) %>%
        dplyr::select( gasDate ,
                       dailyTemp=X4 ,
                       normalTemp=X3) %>%
        dplyr::bind_rows(gasTemp,.)

      if ( rmTmpFiles ){
        file.remove(zipFile,xlsFile)
      }
    }
    # removing future and today lines - they contain forecast from CHMI
    gasTemp <- dplyr::filter(gasTemp, gasDate < Sys.Date() )
  }

  return(gasTemp)
}

