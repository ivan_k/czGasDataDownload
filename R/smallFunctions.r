# actualYearNumeric -------------------------------------------------------
#' Get actual year
#'
#' Function return actuall year in numeric format.
#'
#' @author Ivan Kasanicky, \email{kasanickyivan@@gmail.com}
#'
#' @examples
#'  actualYearNumeric()
#'
#' @import dplyr
#' @export
actualYearNumeric <- function(){
  actYear <- Sys.time() %>%
    strftime(format='%Y') %>%
    as.numeric()
  return(actYear)
}
# getOteFileUrl -------------------------------------------------------
#' Get URL of file on OTE page
#'
#' Function return URL of file located on the OTE servers containing the gas daily avarage temperatures.
#'
#' @param years different years to get according files
#'
#' @return character vector containing URLs for the given files
#'
#' @author Ivan Kasanicky, \email{kasanickyivan@@gmail.com}
#'
#' @examples
#'  getOteFileUrl()
#'
#' @export
getOteFileUrl <- function(years=integer(0)){
  urlPatern <- 'http://www.ote-cr.cz/statistika/typove-diagramy-dodavek-plynu/teploty/attached/%g/Teploty_plyn_%g_CZ.zip'
  firstYear <- 2011 # first year with data available
  actYear <- actualYearNumeric()
  if ( !length(years) ){
    years <- firstYear:actYear
  }
  # controls
  yearsNA <- setdiff( years, firstYear:actYear )
  if ( length(yearsNA) ){
    warning( 'data for following years not available: ',
             paste( yearsNA , collapse = ', ' ), immediate. = T )
  }
  years <- intersect( years, firstYear:actYear )

  links <- character(0)
  if ( length(years) ) {
    for ( i in 1:length(years) ) {
      links[i] <- sprintf( urlPatern, years[i] ,  years[i] )
    }
  }

  return(links)
}

